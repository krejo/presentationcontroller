/*
 From Apple's
   AAPLOverlayPresentationController header.
  
 */

@import UIKit;

@interface PCOverlayPresentationController : UIPresentationController

@property (nonatomic) UIView *dimmingView;

@end
