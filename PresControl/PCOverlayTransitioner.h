/*
 From Apple's
  AAPLOverlayAnimatedTransitioning and AAPLOverlayTransitioningDelegate interfaces.
  
 */

@import UIKit;

@interface PCOverlayAnimatedTransitioning : NSObject <UIViewControllerAnimatedTransitioning>
@property (nonatomic) BOOL isPresentation;
@end

@interface PCOverlayTransitioningDelegate : NSObject <UIViewControllerTransitioningDelegate>
@end