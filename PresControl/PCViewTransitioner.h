//
//  JWCameraAnimatedTransitioning.h
//  JamWDev
//
//  Created by JOSEPH KERR on 6/18/16.
//  Copyright © 2016 JOSEPH KERR. All rights reserved.
//

@import UIKit;

@interface PCViewAnimatedTransitioning : NSObject <UIViewControllerAnimatedTransitioning>
@property (nonatomic) BOOL isPresentation;
@end

@interface PCViewOptionAnimatedTransitioning : NSObject <UIViewControllerAnimatedTransitioning>
@property (nonatomic) BOOL isPresentation;
@end


@interface PCViewTransitioningDelegate : NSObject <UIViewControllerTransitioningDelegate>
@property (nonatomic) UIView *inTransitionView;
@end


