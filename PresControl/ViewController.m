//
//  ViewController.m
//  PresControl
//
//  Created by JOSEPH KERR on 6/19/16.
//  Copyright © 2016 JOSEPH KERR. All rights reserved.
//

#import "ViewController.h"
#import "PCPresentedViewController.h"
#import "PCViewTransitioner.h"
#import "PCRevealTransitioner.h"
#import "PCOverlayTransitioner.h"

@interface ViewController ()

@property (weak, nonatomic) IBOutlet UIButton *presentButton;
@property (nonatomic) id<UIViewControllerTransitioningDelegate> ttransitioningDelegate;
@property (nonatomic) UIView *snapShotView;
@end


@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([segue.identifier isEqualToString:@"PCPresent"]) {
        
        _ttransitioningDelegate = [[PCViewTransitioningDelegate alloc] init];
        [segue.destinationViewController setTransitioningDelegate:[self ttransitioningDelegate]];
        
    } else if ([segue.identifier isEqualToString:@"PCPresentReveal"]) {
        
        _ttransitioningDelegate = [[PCRevealTransitioningDelegate alloc] init];
        
        self.presentButton.selected = NO;
        self.presentButton.highlighted = NO;
        self.snapShotView = [self.view snapshotViewAfterScreenUpdates:YES];
        self.snapShotView.frame = self.view.bounds;
        
        [(PCViewTransitioningDelegate*)_ttransitioningDelegate setInTransitionView:self.snapShotView];
        
        [segue.destinationViewController setTransitioningDelegate:[self ttransitioningDelegate]];
        
    } else if ([segue.identifier isEqualToString:@"PCOverlay"]) {
        
        _ttransitioningDelegate = [[PCOverlayTransitioningDelegate alloc] init];
        [segue.destinationViewController setTransitioningDelegate:[self ttransitioningDelegate]];
        
        [(PCPresentedViewController*)segue.destinationViewController setUseCustomContent:YES];
    }

}

@end


