//
//  PCPresentedViewController.h
//  PresControl
//
//  Created by JOSEPH KERR on 6/19/16.
//  Copyright © 2016 JOSEPH KERR. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PCPresentedViewController : UIViewController

@property (nonatomic) Boolean useCustomContent;

@property (nonatomic) UIVisualEffectView *backgroundView;
@property (nonatomic) UIVisualEffectView *foregroundContentView;
@property (nonatomic) UIScrollView *foregroundContentScrollView;
@property (nonatomic) UIBlurEffect *blurEffect;

@property (nonatomic) UILabel *textLabel;
@property (nonatomic) UIButton *actionButton;

@end
