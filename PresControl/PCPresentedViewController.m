//
//  PCPresentedViewController.m
//  PresControl
//
//  Created by JOSEPH KERR on 6/19/16.
//  Copyright © 2016 JOSEPH KERR. All rights reserved.
//

#import "PCPresentedViewController.h"


@implementation PCPresentedViewController

-(instancetype)initWithCoder:(NSCoder *)aDecoder {
    if (self = [super initWithCoder:aDecoder]) {
        [self setModalPresentationStyle:UIModalPresentationCustom];
    }
    return self;
}

- (IBAction)dismissWithButton:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)savePushed:(id)sender {
    // When saving, set our processed image on the photo view, and dismiss ourselves
    [[self presentingViewController] dismissViewControllerAnimated:YES completion:NULL];
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    if (_useCustomContent) {
        
        _blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleExtraLight];

        _backgroundView = [[UIVisualEffectView alloc] initWithEffect:[self blurEffect]];
        
        [self configureViews];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - custom content

- (void)configureViews {
    
    [[self view] setBackgroundColor:[UIColor clearColor]];
    
    [[self backgroundView] setTranslatesAutoresizingMaskIntoConstraints:NO];
    
    _textLabel = [[UILabel alloc] init];
    [[self textLabel] setText:NSLocalizedString(@"Crazy Text", @"Some Text")];
    [[self textLabel] setTextColor:[UIColor blackColor]];
    
    _actionButton = [UIButton buttonWithType:UIButtonTypeSystem];
    [[self actionButton] setTranslatesAutoresizingMaskIntoConstraints:NO];
    [[self actionButton] setTitle:NSLocalizedString(@"Save", @"Save button title.") forState:UIControlStateNormal];
    [[[self actionButton] titleLabel] setFont:[UIFont systemFontOfSize:32.0]];
    [[self actionButton] addTarget:self action:@selector(savePushed:) forControlEvents:UIControlEventTouchUpInside];
    
    [[self view] addSubview:[self backgroundView]];


    UIView* contentView = _backgroundView.contentView;

    [contentView addSubview:[self textLabel]];
    [contentView addSubview:[self actionButton]];

    
    NSDictionary* views = @{
                            @"backgroundView" : [self backgroundView],
                            @"textLabel" : [self textLabel],
                            @"actionButton" : [self actionButton]

                            };
    
    [[self view] addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[backgroundView]|" options:0 metrics:nil views:views]];
    [[self view] addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[backgroundView]|" options:0 metrics:nil views:views]];
    [[self view] addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[textLabel]-|" options:0 metrics:nil views:views]];
    [[self view] addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[actionButton]-|" options:0 metrics:nil views:views]];
    [[self view] addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:
                                 @"V:|-(>=30)-[textLabel(==28)]-(>=30)-[actionButton]|" options:0 metrics:nil views:views]];
}


@end


//    [[self foregroundContentScrollView] setTranslatesAutoresizingMaskIntoConstraints:NO];
//    [[self foregroundContentView] setTranslatesAutoresizingMaskIntoConstraints:NO];
//        _foregroundContentScrollView = [[UIScrollView alloc] initWithFrame:[[self view] frame]];
//
//        _foregroundContentView = [[UIVisualEffectView alloc] initWithEffect:
//                                  [UIVibrancyEffect effectForBlurEffect:[self blurEffect]]];
//
//
//    [[self view] addSubview:[self foregroundContentView]];
//    [[[self foregroundContentView] contentView] addSubview:[self textLabel]];
//    [[[self foregroundContentView] contentView] addSubview:[self actionButton]];
//
//                            @"foregroundContentView" : [self foregroundContentView],
//
//
//    [[self view] addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[foregroundContentView]|" options:0 metrics:nil views:views]];
//    [[self view] addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[foregroundContentView]|" options:0 metrics:nil views:views]];

