//
//  JWCameraPresentationController.h
//  JamWDev
//
//  Created by JOSEPH KERR on 6/18/16.
//  Copyright © 2016 JOSEPH KERR. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PCViewPresentationController : UIPresentationController

- (instancetype)initWithPresentedViewController:(UIViewController *)presentedViewController presentingViewController:(UIViewController *)presentingViewController transitioningView:(UIView*)transitioningView;

@property (nonatomic) UIView *inTransitionView;

@end
