//
//  PCRevealTransitioner.h
//  PresControl
//
//  Created by JOSEPH KERR on 6/21/16.
//  Copyright © 2016 JOSEPH KERR. All rights reserved.
//


@import UIKit;

@interface PCRevealAnimatedTransitioning : NSObject <UIViewControllerAnimatedTransitioning>
@property (nonatomic) BOOL isPresentation;
@end

@interface PCRevealTransitioningDelegate : NSObject <UIViewControllerTransitioningDelegate>
@property (nonatomic) UIView *inTransitionView;
@end


