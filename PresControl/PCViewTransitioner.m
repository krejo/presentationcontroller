//
//  JWCameraAnimatedTransitioning.m
//  JamWDev
//
//  Created by JOSEPH KERR on 6/18/16.
//  Copyright © 2016 JOSEPH KERR. All rights reserved.
//

#import "PCViewTransitioner.h"
#import "PCViewPresentationController.h"


//#define USE_OPTION

@implementation PCViewTransitioningDelegate

- (UIPresentationController *)presentationControllerForPresentedViewController:(UIViewController *)presented presentingViewController:(UIViewController *)presenting sourceViewController:(UIViewController *)source
{
    // Here, we'll provide the presentation controller to be used for the presentation
    Class presentationControllerClass;
    
    presentationControllerClass = [PCViewPresentationController class];

    return [[presentationControllerClass alloc] initWithPresentedViewController:presented presentingViewController:presenting];
    
//    PCViewPresentationController *pc = [[presentationControllerClass alloc] initWithPresentedViewController:presented presentingViewController:presenting];
    
//    PCViewPresentationController *pc = [[presentationControllerClass alloc] initWithPresentedViewController:presented presentingViewController:presenting transitioningView:self.inTransitionView];
//    
//    return pc;
}


#ifdef USE_OPTION
- (PCViewOptionAnimatedTransitioning *)animationController
#else
- (PCViewAnimatedTransitioning *)animationController
#endif
{
    
#ifdef USE_OPTION
    PCViewOptionAnimatedTransitioning *animationController = [[PCViewOptionAnimatedTransitioning alloc] init];
#else
    PCViewAnimatedTransitioning *animationController = [[PCViewAnimatedTransitioning alloc] init];
#endif
    return animationController;
}

- (id <UIViewControllerAnimatedTransitioning>)animationControllerForPresentedController:(UIViewController *)presented presentingController:(UIViewController *)presenting sourceController:(UIViewController *)source
{
    
    
#ifdef USE_OPTION
    PCViewOptionAnimatedTransitioning *animationController = [self animationController];
#else
    PCViewAnimatedTransitioning *animationController = [self animationController];
#endif
    
    [animationController setIsPresentation:YES];
    
//    NSNumber *value = [NSNumber numberWithInt:UIInterfaceOrientationLandscapeRight];
//    [[UIDevice currentDevice] setValue:value forKey:@"orientation"];

    return animationController;
}

- (id <UIViewControllerAnimatedTransitioning>)animationControllerForDismissedController:(UIViewController *)dismissed
{
    
    
#ifdef USE_OPTION
    PCViewOptionAnimatedTransitioning *animationController = [self animationController];
#else
    PCViewAnimatedTransitioning *animationController = [self animationController];
#endif
    
    [animationController setIsPresentation:NO];
    
    return animationController;
}

@end




@implementation PCViewAnimatedTransitioning

- (NSTimeInterval)transitionDuration:(id <UIViewControllerContextTransitioning>)transitionContext
{
    return 0.5;
}

- (void)animateTransition:(id <UIViewControllerContextTransitioning>)transitionContext
{
    // Here, we perform the animations necessary for the transition
    
    UIViewController *fromVC = [transitionContext viewControllerForKey:UITransitionContextFromViewControllerKey];
    UIView *fromView = [fromVC view];
    UIViewController *toVC   = [transitionContext viewControllerForKey:UITransitionContextToViewControllerKey];
    UIView *toView = [toVC view];
    UIView *containerView = [transitionContext containerView];
    BOOL isPresentation = [self isPresentation];
    
    if(isPresentation) {
        [containerView addSubview:toView];
    }
    
    UIViewController *animatingVC = isPresentation? toVC : fromVC;
    UIView *animatingView = [animatingVC view];
    
    CGRect appearedFrame = [transitionContext finalFrameForViewController:animatingVC];
    
    // Our dismissed frame is the same as our appeared frame, but off the right edge of the container
    CGRect dismissedFrame = appearedFrame;
    dismissedFrame.origin.x += dismissedFrame.size.width;
    
    CGRect initialFrame = isPresentation ? dismissedFrame : appearedFrame;
    CGRect finalFrame = isPresentation ? appearedFrame : dismissedFrame;
    
    [animatingView setFrame:initialFrame];
    
    // Animate using the duration from -transitionDuration:
    [UIView animateWithDuration:[self transitionDuration:transitionContext]
                          delay:0
         usingSpringWithDamping:300.0
          initialSpringVelocity:5.0
                        options:UIViewAnimationOptionAllowUserInteraction | UIViewAnimationOptionBeginFromCurrentState
                     animations:^{
                         [animatingView setFrame:finalFrame];
                     }
                     completion:^(BOOL finished){
                         // If we're dismissing, remove the presented view from the hierarchy
                         if(![self isPresentation])
                         {
                             [fromView removeFromSuperview];
                         }
                         // We need to notify the view controller system that the transition has finished
                         [transitionContext completeTransition:YES];
                     }];
    
    
    
}

@end




@implementation PCViewOptionAnimatedTransitioning

- (NSTimeInterval)transitionDuration:(id <UIViewControllerContextTransitioning>)transitionContext
{
    return 0.5;
}


- (void)animateTransition:(id <UIViewControllerContextTransitioning>)transitionContext
{
    // Here, we perform the animations necessary for the transition
    
    UIViewController *fromVC = [transitionContext viewControllerForKey:UITransitionContextFromViewControllerKey];
    UIView *fromView = [fromVC view];
    UIViewController *toVC   = [transitionContext viewControllerForKey:UITransitionContextToViewControllerKey];
    UIView *toView = [toVC view];
    
    UIView *containerView = [transitionContext containerView];
    
    BOOL isPresentation = [self isPresentation];
    
    
    if(isPresentation)
    {
        [containerView addSubview:toView];
        //[containerView insertSubview:toView atIndex:0];
    }
    
    UIViewController *animatingVC = isPresentation? toVC : fromVC;
    UIView *animatingView = [animatingVC view];
    
    CGRect appearedFrame = [transitionContext finalFrameForViewController:animatingVC];
    
    // Our dismissed frame is the same as our appeared frame, but off the right edge of the container
    CGRect dismissedFrame = appearedFrame;
    //dismissedFrame.origin.x += dismissedFrame.size.width;
    
    CGRect initialFrame = isPresentation ? dismissedFrame : appearedFrame;
    //CGRect finalFrame = isPresentation ? appearedFrame : dismissedFrame;
    
    [animatingView setFrame:initialFrame];
    
    // Animate using the duration from -transitionDuration:
    [UIView animateWithDuration:[self transitionDuration:transitionContext]
                          delay:0
         usingSpringWithDamping:300.0
          initialSpringVelocity:5.0
                        options:UIViewAnimationOptionAllowUserInteraction | UIViewAnimationOptionBeginFromCurrentState
                     animations:^{
                         //[animatingView setFrame:finalFrame];
                     }
                     completion:^(BOOL finished){
                         // If we're dismissing, remove the presented view from the hierarchy
                         if(![self isPresentation])
                         {
                             [fromView removeFromSuperview];
                         }
                         // We need to notify the view controller system that the transition has finished
                         [transitionContext completeTransition:YES];
                     }];
    
}

@end



