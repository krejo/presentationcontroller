//
//  JWCameraPresentationController.m
//  JamWDev
//
//  Created by JOSEPH KERR on 6/18/16.
//  Copyright © 2016 JOSEPH KERR. All rights reserved.
//

#import "PCViewPresentationController.h"
#import "PCViewTransitioner.h"

#define DEGREES_TO_RADIANS(degrees)((M_PI * degrees)/180)


@implementation PCViewPresentationController

- (instancetype)initWithPresentedViewController:(UIViewController *)presentedViewController presentingViewController:(UIViewController *)presentingViewController;
{
    self = [super initWithPresentedViewController:presentedViewController presentingViewController:presentingViewController];
    
    return self;
}


- (instancetype)initWithPresentedViewController:(UIViewController *)presentedViewController presentingViewController:(UIViewController *)presentingViewController transitioningView:(UIView*)transitioningView;
{
    self = [super initWithPresentedViewController:presentedViewController presentingViewController:presentingViewController];
    if(self)
    {
        self.inTransitionView = transitioningView;
    }
    
    return self;
}


- (void)presentationTransitionWillBegin
{
    // Here, we'll set ourselves up for the presentation
    
    UIView* containerView = [self containerView];
    UIViewController* presentedViewController = [self presentedViewController];
    
    // Make sure the dimming view is the size of the container's bounds, and fully transparent
    
    if (_inTransitionView) {
        // Dimmed view
        //[[self inTransitionView] setFrame:[containerView bounds]];
        //[[self inTransitionView] setAlpha:0.0];
        
        [[self inTransitionView] setFrame:[containerView bounds]];
        
        //CGAffineTransform transform = CGAffineTransformMakeRotation(3.14159/2);
        //    CGAffineTransform transform = CGAffineTransformMakeRotation(DEGREES_TO_RADIANS(270));
        //    self.inTransitionView.transform = transform;
        
        // Insert the dimming view below everything else
        
        //[containerView insertSubview:[self inTransitionView] atIndex:0];
        [containerView addSubview:[self inTransitionView]];
    }
    
    CGRect appearedFrame = [containerView bounds];
    
    // Our dismissed frame is the same as our appeared frame, but off the right edge of the container
    CGRect dismissedFrame = appearedFrame;
    dismissedFrame.origin.x += dismissedFrame.size.width - 60;
    
//    CGRect initialFrame = isPresentation ? dismissedFrame : appearedFrame;
//    CGRect finalFrame = isPresentation ? appearedFrame : dismissedFrame;

    CGRect initialFrame = [containerView bounds];
    CGRect finalFrame = dismissedFrame;

    [[self inTransitionView] setFrame:initialFrame];
    
    
    if([presentedViewController transitionCoordinator])
    {
        [[presentedViewController transitionCoordinator] animateAlongsideTransition:^(id<UIViewControllerTransitionCoordinatorContext> context) {
            
            // Fade the dimming view to be fully visible
            
            //[[self inTransitionView] setAlpha:1.0];
            
            [[self inTransitionView] setFrame:finalFrame];
            
        } completion:nil];
    }
    else
    {
        [[self inTransitionView] setAlpha:1.0];
    }
}

- (void)dismissalTransitionWillBegin
{
    // Here, we'll undo what we did in -presentationTransitionWillBegin. Fade the dimming view to be fully transparent
    
    UIView* containerView = [self containerView];
    
    CGRect finalFrame = [containerView bounds];

    
    if([[self presentedViewController] transitionCoordinator])
    {
        [[[self presentedViewController] transitionCoordinator] animateAlongsideTransition:^(id<UIViewControllerTransitionCoordinatorContext> context) {
            
            [[self inTransitionView] setFrame:finalFrame];
            //[[self inTransitionView] setAlpha:0.0];
        } completion:^(id<UIViewControllerTransitionCoordinatorContext>  _Nonnull context) {
            
            [[self inTransitionView] removeFromSuperview];
        }];
    }
    else
    {
        [[self inTransitionView] setAlpha:0.0];
    }
}

- (UIModalPresentationStyle)adaptivePresentationStyle
{
    // When we adapt to a compact width environment, we want to be over full screen
    return UIModalPresentationOverFullScreen;
}

- (CGSize)sizeForChildContentContainer:(id <UIContentContainer>)container withParentContainerSize:(CGSize)parentSize
{
    // We always want a size that's a fraction of our parent view width, and just as tall as our parent
    
    //return CGSizeMake(floorf(parentSize.width / 3.0),parentSize.height);
    return CGSizeMake(floorf(parentSize.width * 0.60),parentSize.height);
    
    //return CGSizeMake(floorf(parentSize.width),parentSize.height);
}

- (void)containerViewWillLayoutSubviews
{
    // Before layout, make sure our dimmingView and presentedView have the correct frame
    //[[self inTransitionView] setFrame:[[self containerView] bounds]];
    [[self presentedView] setFrame:[self frameOfPresentedViewInContainerView]];
}

- (BOOL)shouldPresentInFullscreen
{
    // This is a full screen presentation
    return YES;
}

- (CGRect)frameOfPresentedViewInContainerView
{
    // Return a rect with the same size as -sizeForChildContentContainer:withParentContainerSize:, and right aligned
    CGRect presentedViewFrame = CGRectZero;
    CGRect containerBounds = [[self containerView] bounds];
    
    presentedViewFrame.size =
    [self sizeForChildContentContainer:(UIViewController<UIContentContainer> *)[self presentedViewController]
               withParentContainerSize:containerBounds.size];
    
    presentedViewFrame.origin.x = containerBounds.size.width - presentedViewFrame.size.width;
    
    return presentedViewFrame;
}


@end

