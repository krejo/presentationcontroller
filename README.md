# README #

An iOS App written in Objective-C 2 to demo UIPresentation controller and transitioning delegate.

### Purpose ###

* UIPresentation Controller demo
* Version 1

### Setup ###

* Open Xcode project in Xcode7
* Modify the storyboard name segue 'PCOverlay'
* Modify the storyboard name segue 'PCPresentReveal'
* Modify the storyboard name segue 'PCPresent'

ViewController has a button connected to by segue to a custom ViewController PCPresentedViewController.  The prepareForSegue will configure the transition and setup 

### PCOverlay ###

PCOverlay will present an overlay viewController covering a small portion of the screen, press the button on the presented view controller, or tap the inactive greyed out area that reveals the presenting viewcontroller beneath it

* The overlay takes the entire screen and has tap to dismiss
* The content of the presented viewController is a fraction of the screen
* 


![Alt text](/screens/PresentOverlaySetting.png?raw=true "PresentOverlay")


### PCPresentReveal ###
PCPresentReveal will present the presented view controller in hamburger style

* A snapshot view of the presenting viewController is taken
* The snapshot is added to the presented
* And slid mostly out of view to reveal the presented viewController

### PCPresentReveal ###
PCPresent will present the presented view controller covering a portion of the screen
Rotation should yield same proportion

* Simply the presented viewController is positioned and sized to a portion of the screen
* Press the button on the presented view controller to dismiss it


### Who do I talk to? ###
* Repo owner or admin
* Other community or team contact